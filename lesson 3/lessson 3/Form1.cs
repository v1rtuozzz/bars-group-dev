﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lessson_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            #region условия 
            // || (OR) && (AND)
            //var a = int.Parse(textBox1.Text);
            //var b = int.Parse(textBox2.Text);
            //var c = int.Parse(textBox3.Text);
            //var d = int.Parse(textBox4.Text);
            //var str = String.Empty;
            //if (a > b && c>d)
            //{
            //    str = "левая часть больше правой";
            //}
            //else if (a < b && c<d)
            //{
            //    str = "правая часть больше правой";
            //}
            //else
            //{
            //    str = "хаос";
            //}

            //str = "c";
            //switch (str)
            //{
            //    case "a":
            //        str = "1";
            //        break;
            //    case "b":
            //        str = "2";
            //        break;
            //    case "c":
            //        str = "3";
            //        break;
            //    case "d":
            //        str = "4";
            //        break;
            //    default:
            //        str = "not value";
            //        break;
            //}
            #endregion
            #region foreach
            //var lst = new List<string>();
            //lst.Add("1");
            //lst.Add("2");
            //lst.Add("3");
            //foreach (var v in lst)
            //{
            //    textBox5.Text += v;
            //}
            #endregion
            //for (int i = 0; i<10; i++)
            //{
            //    textBox5.Text += i;
            //}
            //while (a < b)
            //{
            //    textBox5.Text += a;
            //    a++;

            //}
            try
            {
                var a = int.Parse(textBox1.Text);
                var b = int.Parse(textBox2.Text);
                var c = int.Parse(textBox3.Text);
                var d = int.Parse(textBox4.Text);
            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
