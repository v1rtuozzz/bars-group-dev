﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace lesson_6___API.Controllers
{
    public class TestController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage MyMethod()

        {
            var humans = new List<Human>();
            var human = new Human()
            {
                Age = 20,
                Name = "Adam"
            };
            var human2 = new Human()
            {
                Age = 18,
                Name = "Eva"
            };
            humans.Add(human);
            humans.Add(human2);

            return Request.CreateResponse(HttpStatusCode.OK, humans, Configuration.Formatters.JsonFormatter);

        }

        [HttpPost]
        public HttpResponseMessage GetUsers([FromBody] Human human)
        {   
            if (human != null && human.Age < 20)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "You shall not pass", Configuration.Formatters.JsonFormatter);
            }
            var humans = new List<Human>();
            humans.Add(human);
            return Request.CreateResponse(HttpStatusCode.OK, humans, Configuration.Formatters.JsonFormatter);
        }

        public class Human
        {
            public int Age { get; set; }
            public string Name { get; set; }
        }
    }
}
