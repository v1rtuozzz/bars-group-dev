﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.IO;

namespace homework_4
{
    class Database
    {
        public SQLiteConnection Connect;
        public string connectionstring { get; set; }
        string connection;

        public void getconnection()
        {
            connection = @"Data Source=Users.db; Version=3";
            connectionstring = connection;
        }

        public Database()
        {
            if (!File.Exists("./Users.db"))
            {
                SQLiteConnection.CreateFile("Users.db");
                getconnection();

                using(SQLiteConnection con = new SQLiteConnection(connection))
                {
                    SQLiteCommand command = new SQLiteCommand();
                    con.Open();
                    string query = @"CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, firstname VARCHAR(100), lastname VARCHAR(100), login VARCHAR(100), password VARCHAR(30))";
                    command.CommandText = query;
                    command.Connection = con;
                    command.ExecuteNonQuery();
                    con.Close();
                }
            }
            else
            {
                return;
            }    


        }
    }
}
