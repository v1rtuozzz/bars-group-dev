﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace homework_4
{
    public partial class LoginForm : Form
    {
        Database Data;
        public LoginForm()
        {
            InitializeComponent();
        }

        private void enterButton_Click(object sender, EventArgs e)
        {       // Валидация логина и пароля
                if (loginField.Text != string.Empty && passField.Text != string.Empty)
                {
                    Data = new Database();
                    Data.getconnection();
                    try
                    {
                        using (SQLiteConnection con = new SQLiteConnection(Data.connectionstring))
                        {
                            SQLiteCommand command = new SQLiteCommand();
                            con.Open();
                            int count = 0;
                            string query = @"SELECT * FROM user WHERE login = '" + loginField.Text + "' AND password = '" + passField.Text + "'";
                            command.CommandText = query;
                            command.Connection = con;
                            SQLiteDataReader reader = command.ExecuteReader();
                            while (reader.Read())
                            {
                                count++;
                            }
                            //если count > 0, значит логин есть в БД
                            if (count != 0)
                            {
                                MessageBox.Show("Вы авторизованы");
                                return;
                            }
                            else
                            {
                                MessageBox.Show("Ошибка авторизации");
                            }
                        }
                    }
                catch
                {
                    return;
                }
            }
                
            }

        #region buttons
        private void loginCloseButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        Point lastPoint;
        private void loginPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void loginPanel_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        #endregion

        private void registerLabel_Click(object sender, EventArgs e)
        {
            this.Hide();
            RegisterForm registerForm = new RegisterForm();
            registerForm.Show();
        }
    }

    }




