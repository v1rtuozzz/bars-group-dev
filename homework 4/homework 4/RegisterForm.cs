﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace homework_4
{
    public partial class RegisterForm : Form
    {
        Database Data;
        public RegisterForm()
        {
            InitializeComponent();
        }

        private bool insert(string firstname, string lastname, string login, string password)
        {
            Data = new Database();
            Data.getconnection();
            
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(Data.connectionstring))
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand();
                    string query = @"INSERT INTO user (firstname, lastname, login, password) VALUES (@firstname, @lastname, @login, @password)";
                    command.CommandText = query;
                    command.Connection = con;
                    command.Parameters.AddWithValue("@firstname", firstnameField.Text);
                    command.Parameters.AddWithValue("@lastname", lastnameField.Text);
                    command.Parameters.AddWithValue("@login", loginField.Text);
                    command.Parameters.AddWithValue("@password", passField.Text);
                    command.ExecuteNonQuery();
                    MessageBox.Show("Вы успешно зарегистрировались");
                    con.Close();
                    return true;
                }
            }
            catch
            {
                MessageBox.Show("Ошибка регистрации");
                return false;
            }
        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            // Проверка заполнения обязательных полей

            var isInsert = false;
            if (loginField.Text != string.Empty && passField.Text != string.Empty)
            {
                isInsert = insert(firstnameField.Text, lastnameField.Text, loginField.Text, passField.Text);
            }
            else
            {
                MessageBox.Show("Пожалуйста, заполните поля");
            }
            // Проверка логина

            if (!isInsert !=false && loginField.Text != string.Empty && passField.Text != string.Empty)
            {
                Data = new Database();
                Data.getconnection();
                try
                {
                    using (SQLiteConnection con = new SQLiteConnection(Data.connectionstring))
                    {
                        SQLiteCommand command = new SQLiteCommand();
                        con.Open();
                        int count = 0;
                        string query = @"SELECT * FROM user WHERE login = '" + loginField.Text + "'";
                        command.CommandText = query;
                        command.Connection = con;
                        SQLiteDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            count++;
                        }
                        //если count > 0, значит такой логин уже есть в БД
                        if (count != 0)
                        {
                            MessageBox.Show("Логин уже занят. Введите другой");
                            return;
                        }
                        else
                        {
                            MessageBox.Show("Вы успешно зарегистрировались");
                        }

                    }
                }
                catch
                {
                    return;
                }
            }
        }
        #region кнопки
        private void closeButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        Point lastPoint;
        private void regPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void regPanel_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        private void registerLabel_Click(object sender, EventArgs e)
        {
            this.Hide();
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
        }
        #endregion

    }
}
