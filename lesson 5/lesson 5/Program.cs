﻿using System;
using System.Text.Json;

namespace lesson_5
{
    class Program
    {
        static void Main(string[] args)
        {
            //var a = new Human();
            //a.SetName("Ramil");
            //a.CheckAge(17);
            //a.Go();
            //var b = new Student();
            //b.Age = 20;
            //b.SetName("Tom");
            //b.Group = "123";
            //b.Go();
            //Console.ReadLine();
            var jsonTest = new JSONTest();
            jsonTest.Adress = "Russia";
            jsonTest.Age = 20;
            jsonTest.Name = "Tom";
            var jsonSerialize = JsonSerializer.Serialize<JSONTest>(jsonTest);
            Console.WriteLine(jsonSerialize);
            var jsonT = JsonSerializer.Deserialize<JSONTest>(jsonSerialize);
            var jsonT2 = JsonSerializer.Deserialize<JSONTest>("{\"Name\":\"Bill\",\"Age\":60,\"Adress\":\"Poland\"}");
            Console.ReadLine();
        }
    }
}
