﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lesson_5
{
    class Human     
    {
        public string Name { get; set; }
        public void SetName (string name)
        {
            Name = name;
        }
        public int Age { get; set; }
        private bool IsAgeCorrect { get; set; }

        public void Go()
        {
            Console.WriteLine("I am " + Name + ". I go");
            Sell();
        }
        public void CheckAge(int Age)
        {
            if (Age > 18)
            {
                IsAgeCorrect = true;
            }
            else
            {
                IsAgeCorrect = false;
            }
        }
        private void Sell()
        {
            if (IsAgeCorrect) Console.WriteLine("Товар можно продавать");
            else Console.WriteLine("Товар нельзя продавать");
        }
    }
}
