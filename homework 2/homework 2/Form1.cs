﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Homework_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button2.Text = "Hi";
            button3.Text = "Hello";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button1.Text = "How are you";
            button3.Text = "What's up?";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button1.Text = "I'm fine";
            button2.Text = "Dope";
        }

        private void goButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"{button1.Text}, {button2.Text}, {button3.Text}");
        }
    }
}
