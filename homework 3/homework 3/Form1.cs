﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace homework_3
{
    public partial class Form1 : Form
    {
        string operation { get => operationCombobox.SelectedItem.ToString(); }
        public Form1()
        {
            InitializeComponent();
            operationCombobox.SelectedIndex = 0;
        }

        private void colorsOutput_click(object sender, EventArgs e)
        {
            List<string> colors = new List<string>() { "белый", "черный", "красный", "синий", "желтый", "зеленый", "фиолетовый" };

            foreach (var v in colors)
            {
                if (v == "синий")
                {
                    continue;
                }
                textBox1.Text += v + ", ";
            }
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            //var operation = operationCombobox.SelectedItem.ToString();
            var a = int.Parse(number1.Text);
            var b = int.Parse(number2.Text);
            var c = 0;

            if (operation == "+")
                c = a + b;
            else if (operation == "-")
                c = a - b;
            else if (operation == "*")
                c = a * b;
            else if (operation == "/")
            {
                if (b != 0)
                    c = a / b;
                else
                    MessageBox.Show("Нельзя делить на 0");
            }
            resultTextBox.Text = c.ToString();

        }
 
    }
}
