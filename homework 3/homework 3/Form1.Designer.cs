﻿
namespace homework_3
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorsOutput = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.number1 = new System.Windows.Forms.TextBox();
            this.number2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.resultTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.operationCombobox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // colorsOutput
            // 
            this.colorsOutput.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.colorsOutput.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.colorsOutput.Location = new System.Drawing.Point(18, 32);
            this.colorsOutput.Name = "colorsOutput";
            this.colorsOutput.Size = new System.Drawing.Size(116, 65);
            this.colorsOutput.TabIndex = 0;
            this.colorsOutput.Text = "Вывести все цвета, кроме синего";
            this.colorsOutput.UseVisualStyleBackColor = false;
            this.colorsOutput.Click += new System.EventHandler(this.colorsOutput_click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Window;
            this.textBox1.Location = new System.Drawing.Point(173, 74);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(379, 23);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(18, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 65);
            this.label1.TabIndex = 2;
            this.label1.Text = "Калькулятор";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Zilla Slab", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(173, 174);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 19);
            this.label2.TabIndex = 6;
            this.label2.Text = "a";
            // 
            // number1
            // 
            this.number1.Location = new System.Drawing.Point(173, 196);
            this.number1.Name = "number1";
            this.number1.Size = new System.Drawing.Size(55, 23);
            this.number1.TabIndex = 3;
            // 
            // number2
            // 
            this.number2.Location = new System.Drawing.Point(340, 196);
            this.number2.Name = "number2";
            this.number2.Size = new System.Drawing.Size(55, 23);
            this.number2.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Zilla Slab", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(340, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 19);
            this.label3.TabIndex = 7;
            this.label3.Text = "b";
            // 
            // resultTextBox
            // 
            this.resultTextBox.Location = new System.Drawing.Point(488, 196);
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.Size = new System.Drawing.Size(55, 23);
            this.resultTextBox.TabIndex = 10;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(422, 196);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(38, 23);
            this.calculateButton.TabIndex = 15;
            this.calculateButton.Text = "=";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // operationCombobox
            // 
            this.operationCombobox.FormattingEnabled = true;
            this.operationCombobox.Items.AddRange(new object[] {
            "+",
            "-",
            "/",
            "*"});
            this.operationCombobox.Location = new System.Drawing.Point(257, 196);
            this.operationCombobox.Name = "operationCombobox";
            this.operationCombobox.Size = new System.Drawing.Size(55, 23);
            this.operationCombobox.TabIndex = 16;
            // 
            // Form1
            // 
            this.AccessibleName = "";
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 320);
            this.Controls.Add(this.operationCombobox);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.resultTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.number2);
            this.Controls.Add(this.number1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.colorsOutput);
            this.Name = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button colorsOutput;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox number1;
        private System.Windows.Forms.TextBox number2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox resultTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.ComboBox operationCombobox;
    }
}

