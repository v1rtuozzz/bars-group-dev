﻿
namespace lesson_4___sqlite
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.createTableButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.copyToDBButton = new System.Windows.Forms.Button();
            this.getDataButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // createTableButton
            // 
            this.createTableButton.Location = new System.Drawing.Point(27, 26);
            this.createTableButton.Name = "createTableButton";
            this.createTableButton.Size = new System.Drawing.Size(152, 36);
            this.createTableButton.TabIndex = 0;
            this.createTableButton.Text = "Создать таблицу";
            this.createTableButton.UseVisualStyleBackColor = true;
            this.createTableButton.Click += new System.EventHandler(this.createTableButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(27, 86);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(152, 23);
            this.textBox1.TabIndex = 1;
            // 
            // copyToDBButton
            // 
            this.copyToDBButton.Location = new System.Drawing.Point(27, 134);
            this.copyToDBButton.Name = "copyToDBButton";
            this.copyToDBButton.Size = new System.Drawing.Size(152, 36);
            this.copyToDBButton.TabIndex = 2;
            this.copyToDBButton.Text = "Записать в БД";
            this.copyToDBButton.UseVisualStyleBackColor = true;
            this.copyToDBButton.Click += new System.EventHandler(this.copyToDBButton_Click);
            // 
            // getDataButton
            // 
            this.getDataButton.Location = new System.Drawing.Point(27, 200);
            this.getDataButton.Name = "getDataButton";
            this.getDataButton.Size = new System.Drawing.Size(152, 36);
            this.getDataButton.TabIndex = 3;
            this.getDataButton.Text = "Получить данные";
            this.getDataButton.UseVisualStyleBackColor = true;
            this.getDataButton.Click += new System.EventHandler(this.getDataButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(256, 296);
            this.Controls.Add(this.getDataButton);
            this.Controls.Add(this.copyToDBButton);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.createTableButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button createTableButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button copyToDBButton;
        private System.Windows.Forms.Button getDataButton;
    }
}

