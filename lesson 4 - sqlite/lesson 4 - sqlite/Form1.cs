﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lesson_4___sqlite
{
    public partial class Form1 : Form
        
    {
        private string _dbSqlite = @"C:\tools\sqlite\TestDB.db";
        
        public Form1()
        {
            InitializeComponent();
            if (!File.Exists(_dbSqlite))
            {
                SQLiteConnection.CreateFile(_dbSqlite);
            }
        }

        private void createTableButton_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection connect = new SQLiteConnection(@"Data Source="+ _dbSqlite + "; Vesrion=3;"))
            {
                var commandText = "CREATE TABLE IF NOT EXISTS [test] ([id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [name] VARCHAR(200))";
                var command = new SQLiteCommand(commandText, connect);
                connect.Open();
                command.ExecuteNonQuery();
                connect.Close();
            }
        }

        private void copyToDBButton_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection connect = new SQLiteConnection(@"Data Source=" + _dbSqlite + "; Vesrion=3;"))
            {
                var commandText = "INSERT INTO [test] ([name]) VALUES (@name)";
                var command = new SQLiteCommand(commandText, connect);
                command.Parameters.AddWithValue("@name", textBox1.Text);
                connect.Open();
                command.ExecuteNonQuery();
                connect.Close();
            }
        }

        private void getDataButton_Click(object sender, EventArgs e)
        {
            var names = new List<string>();
            using (SQLiteConnection connect = new SQLiteConnection(@"Data Source=" + _dbSqlite + "; Vesrion=3;"))
            {
                connect.Open();
                var command = new SQLiteCommand
                {
                    Connection = connect,
                    CommandText = @"SELECT name FROM [test]"
                };
                SQLiteDataReader sqlReader = command.ExecuteReader();
                while (sqlReader.Read())
                {
                    names.Add(sqlReader["name"].ToString());
                }
                connect.Close();
            }
                MessageBox.Show(string.Join(", ", names));
            
        }

        
    }
}
