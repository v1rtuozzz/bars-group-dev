﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework_5
{
    public class Animal
    {
        public Animal(string name, int weight)
        {
            this.Name = name;
            this.Weight = weight;
            this.Id = Guid.NewGuid();

        }
        public Guid Id { get; set; }
        public string TypeAnimal { get; set; }
        public string Name { get; set; }
        private string Weight { get; set; }

        public virtual void Eating(string text)
        {
            Console.WriteLine("Ест");
        }

        private void Breathing()
        {
            Console.WriteLine("Дышит легкими");
        }
    }
}
